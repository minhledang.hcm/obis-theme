import { URL_ORG } from "../../environment";
import { handleResponse } from "../utils";

export const checkOrganization = (orgID: string) =>
	handleResponse(fetch(`${URL_ORG}/${orgID}/metadata`));
