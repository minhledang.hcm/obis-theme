<#macro emailLayout>
<html>
<body>
    <p style="margin-top:0px;margin-bottom: 0px">
      Hi ${user.getFirstName()!""} ${user.getLastName()!""},
    </p>
    <p style="margin-top:0px;margin-bottom: 0px">Welcome to On Hand BI!</p>
    <#nested>
    <img style="margin-top:0px;margin-bottom: 0px" width="80" height="80" src="https://admin.onhandbi.com/logo/main_logo.png"/>
    <p style="margin-top:0px;margin-bottom: 0px">Click <a href="https://admin.onhandbi.com">here</a> to login</p>
    <p style="margin-top:13px;margin-bottom: 0px">Time to share your Power BI,</p>
    <p style="margin-top:0px;margin-bottom: 0px">On Hand Business Intelligence</p>
</body>
</html>
</#macro>
