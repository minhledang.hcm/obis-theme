
import { Meta, StoryObj } from '@storybook/react';
import { createPageStory } from "../createPageStory";

const { PageStory } = createPageStory({
  pageId: "account.ftl"
})

const meta = {
  title: "account/Account",
  component: PageStory,
} satisfies Meta<typeof PageStory>; 
export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  render: () => <PageStory
    kcContext={{
      message: { type: "success", summary: "This is a test message" }
    }}
  />
};