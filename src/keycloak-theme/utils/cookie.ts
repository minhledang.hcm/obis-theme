function extractHostname(url: string , tld: boolean = false) {
  let hostname;

  //find & remove protocol (http, ftp, etc.) and get hostname
  if (url.indexOf("://") > -1) {
      hostname = url.split('/')[2];
  }else {
      hostname = url.split('/')[0];
  }

  //find & remove port number
  hostname = hostname.split(':')[0];

  //find & remove "?"
  hostname = hostname.split('?')[0];

  if(tld){
    let hostnames = hostname.split('.');
    hostname = hostnames[hostnames.length-2] + '.' + hostnames[hostnames.length-1];
  }

  return hostname;
}

const rootHostName = extractHostname(window.location.hostname, true)

export function setCookie(key: string, value?: string, days: number = 1) {
	if (value) {
		const date = new Date();
		date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);

		const expires = `expires=${date.toUTCString()}`;
		document.cookie = `${key}=${value};${expires};domain=.${rootHostName};path=/`;
	}
}

export function getCookie(key: string) {
	const name = `${key}=`;
	const decodedCookie = decodeURIComponent(document.cookie);
	const ca = decodedCookie.split(";");
	for (let i = 0; i < ca.length; i++) {
		const c = ca[i];
		if (c.trim().indexOf(name) === 0) {
			return c.trim().substring(name.length, c.length);
		}
	}
	return "";
}

export function deleteCookie(key: string) {
	document.cookie = `${key}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.${rootHostName}; path=/;`;
}
