import {
	ActionGroup,
	Bullseye,
	Button,
	ButtonType,
	Card,
	Form,
	FormGroup,
	FormHelperText,
	Grid,
	GridItem,
	HelperText,
	HelperTextItem,
	InputGroup,
	InputGroupText,
	Text,
	TextContent,
	TextInput,
	TextVariants,
} from "@patternfly/react-core";
import React, { FormEvent, useState } from "react";
import { EnterpriseIcon } from "@patternfly/react-icons";
import { ADMIN_URL, SSO_REALM } from "../../../../environment";
import Logo from "../../../components/logo/organization.logo";
import { checkOrganization } from "../../../services/organization.service";
import { setCookie } from "../../../utils";
import { RLM_COOKIE_KEY } from "../../../constants";

type SsoSectionProps = {
	toLogin: () => void;
};

function login(org: string) {
	setCookie(RLM_COOKIE_KEY, org.toLowerCase());
	location.replace(ADMIN_URL);
}

export default function SsoSection(props: SsoSectionProps) {
	const [state, setState] = useState({
		value: "",
		error: "",
	});

	const onOrganizationChange = (
		_event: FormEvent<HTMLInputElement>,
		value: string
	) => {
		setState({ value, error: "" });
	};

	const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		if (!state.value) {
			setState({ ...state, error: "Please enter an organization" });
			return;
		}

		if (state.value === SSO_REALM) return login(state.value);

		checkOrganization(state.value)
			.then(() => login(state.value))
			.catch(() => setState({ ...state, error: "Organization not found" }));
	};

	return (
		<div className="px-4 flex justify-evenly items-center h-screen min-w-screen bg-[#EBEDEF]">
			<Card isRounded isFlat className="w-full sm:w-[506px] md:w-[768px]">
				<Grid>
					<GridItem md={7} className="py-8 px-6 border-r">
						<Bullseye>
							<Form className="!gap-0 w-full p-4" onSubmit={onSubmit}>
								<TextContent>
									<Text component={TextVariants.h1} className="!mb-0">
										Sign In
									</Text>
									<Text
										component={TextVariants.small}
										className="mb-4 !text-base italic">
										Log in to your business
									</Text>
								</TextContent>
								<FormGroup isRequired label="Business" fieldId="organization">
									<InputGroup>
										<InputGroupText>
											<EnterpriseIcon />
										</InputGroupText>
										<TextInput
											id="organization"
											type="text"
											validated={state.error ? "error" : "default"}
											isRequired
											value={state.value}
											onChange={onOrganizationChange}
										/>
									</InputGroup>
									{state.error && (
										<FormHelperText className="!visible !opacity-100">
											<HelperText>
												<HelperTextItem variant="error">
													{state.error}
												</HelperTextItem>
											</HelperText>
										</FormHelperText>
									)}
								</FormGroup>
								<ActionGroup className="mb-4">
									<Button isBlock type={ButtonType.submit}>
										Continue
									</Button>
								</ActionGroup>
								<TextContent>
									<Text
										className="text-left"
										component={TextVariants.a}
										isVisitedLink
										onClick={props.toLogin}>
										Back to Login
									</Text>
									<Text
										component={TextVariants.small}
										className="mt-12 text-center !text-xs">
										&copy; OHBI. All rights reserved - ver 1.0.0.
									</Text>
								</TextContent>
							</Form>
						</Bullseye>
					</GridItem>
					<GridItem md={5} className="p-4 hidden md:block">
						<Bullseye>
							<Logo />
						</Bullseye>
					</GridItem>
				</Grid>
			</Card>
		</div>
	);
}
