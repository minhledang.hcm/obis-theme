import type { Meta } from "@storybook/react";
import { createPageStory } from "../createPageStory";

const pageId = "info.ftl";

const { PageStory } = createPageStory({ pageId });

const meta = {
    title: "login/info.tfl",
    component: PageStory,
    parameters: {
        viewMode: "story",
        previewTabs: {
            "storybook/docs/panel": {
                "hidden": true
            }
        }
    }
} satisfies Meta<typeof PageStory>;

export default meta;

export const Default = () => (
    <PageStory
        kcContext={{
            message: {
                summary: "This is the server message",
                type: "info"
            }
        }}
    />
);