// @ts-nocheck
// ejected using 'npx eject-keycloak-page'
import { useState } from "react";
import { clsx } from "keycloakify/tools/clsx";
import { UserProfileFormFields } from "./shared/UserProfileFormFields";
import type { PageProps } from "keycloakify/login/pages/PageProps";
import { useGetClassName } from "keycloakify/login/lib/useGetClassName";
import type { KcContext } from "../kcContext";
import type { I18n } from "../i18n";

export default function RegisterUserProfile(
	props: PageProps<
		Extract<KcContext, { pageId: "register-user-profile.ftl" }>,
		I18n
	>
) {
	const { kcContext, i18n, doUseDefaultCss, Template, classes } = props;

	const { getClassName } = useGetClassName({
		doUseDefaultCss,
		classes,
	});

	const { url, messagesPerField, recaptchaRequired, recaptchaSiteKey } =
		kcContext;

	const { msg, msgStr } = i18n;

	const [isFormSubmittable, setIsFormSubmittable] = useState(false);

	return (
		<Template
			{...{ kcContext, i18n, doUseDefaultCss, classes }}
			displayMessage={messagesPerField.exists("global")}
			// displayRequiredFields={true}
			displayWide
			headerNode="Register User Profile"
			imageNode={
				<div className="form-logo-container">
					<div className="row">
						<div className="row">
							<div className="col-xs-6">
								<img
									src="https://admin.onhandbi.com/static/media/ellipse2425115-5gl-200w.5c6cbd0d8cdbbe2c0776d89e7608c88c.svg"
									width={150}
									height={200}
								/>
							</div>
							<div className="col-xs-6">
								<img
									src="https://admin.onhandbi.com/static/media/rectangle3445117-d7b-200w.a761be04aa77aab6a04b1a5f192a879d.svg"
									width={150}
									height={200}
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-xs-6">
								<img
									src="https://admin.onhandbi.com/static/media/free5104-voeh.041f766659c56f2cc07da6733f14344a.svg"
									width={150}
									height={200}
								/>
							</div>
							<div className="col-xs-6">
								<img
									src="https://admin.onhandbi.com/static/media/rectangle3485130-ek5f-900w.bc2238df167aa456d45f4037e4f80ba0.svg"
									width={150}
									height={200}
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-xs-6">
								<img
									src="https://admin.onhandbi.com/static/media/union5114-89lf.cbca4c18350f0f8799b3710a7438c42a.svg"
									width={150}
									height={200}
								/>
							</div>
							<div className="col-xs-6">
								<img
									src="https://admin.onhandbi.com/static/media/vector5118-3w7e.d0131a28292238be603be644fc26e38f.svg"
									width={150}
									height={200}
								/>
							</div>
						</div>
						<div className="row">
							<div className="col-xs-6">
								<img
									src="https://admin.onhandbi.com/static/media/vector1055119-81ua.5656422ff1eed903d8ff0014538f5cca.svg"
									width={150}
									height={200}
								/>
							</div>
						</div>
					</div>
				</div>
			}>
			<form
				id="kc-register-form"
				className={getClassName("kcFormClass")}
				action={url.registrationAction}
				method="post">
				<UserProfileFormFields
					kcContext={kcContext}
					onIsFormSubmittableValueChange={setIsFormSubmittable}
					i18n={i18n}
					getClassName={getClassName}
				/>
				{recaptchaRequired && (
					<div className="form-group">
						<div className={getClassName("kcInputWrapperClass")}>
							<div
								className="g-recaptcha"
								data-size="compact"
								data-sitekey={recaptchaSiteKey}
							/>
						</div>
					</div>
				)}
				<div
					className={getClassName("kcFormGroupClass")}
					style={{ marginBottom: 30 }}>
					<div
						id="kc-form-options"
						className={getClassName("kcFormOptionsClass")}>
						<div className={getClassName("kcFormOptionsWrapperClass")}>
							<span>
								<a href={url.loginUrl}>{msg("backToLogin")}</a>
							</span>
						</div>
					</div>

					<div
						id="kc-form-buttons"
						className={getClassName("kcFormButtonsClass")}>
						<input
							className={clsx(
								getClassName("kcButtonClass"),
								getClassName("kcButtonPrimaryClass"),
								getClassName("kcButtonBlockClass"),
								getClassName("kcButtonLargeClass")
							)}
							type="submit"
							value={msgStr("doRegister")}
							disabled={!isFormSubmittable}
						/>
					</div>
				</div>
			</form>
		</Template>
	);
}
