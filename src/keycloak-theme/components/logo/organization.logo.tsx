const OrganizationLogo = () => {
	return (
		<svg
			width="278"
			height="277"
			viewBox="0 0 278 277"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<g filter="url(#filter0_d_5_1171)">
				<rect
					opacity="0.8"
					x="50.9487"
					y="48.9388"
					width="176.333"
					height="176.333"
					rx="35.2667"
					fill="white"
				/>
				<rect
					x="75.9292"
					y="78.3279"
					width="36.2558"
					height="117.555"
					rx="11.7556"
					fill="url(#paint0_linear_5_1171)"
				/>
				<rect
					x="121.589"
					y="119.112"
					width="36.2558"
					height="76.7709"
					rx="11.7556"
					fill="url(#paint1_linear_5_1171)"
				/>
				<rect
					x="167.25"
					y="146.702"
					width="36.2558"
					height="49.1814"
					rx="11.7556"
					fill="url(#paint2_linear_5_1171)"
				/>
			</g>
			<defs>
				<filter
					id="filter0_d_5_1171"
					x="0.987621"
					y="0.447178"
					width="276.255"
					height="276.256"
					filterUnits="userSpaceOnUse"
					color-interpolation-filters="sRGB"
				>
					<feFlood flood-opacity="0" result="BackgroundImageFix" />
					<feColorMatrix
						in="SourceAlpha"
						type="matrix"
						values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
						result="hardAlpha"
					/>
					<feOffset dy="1.46944" />
					<feGaussianBlur stdDeviation="24.9806" />
					<feComposite in2="hardAlpha" operator="out" />
					<feColorMatrix
						type="matrix"
						values="0 0 0 0 0.339365 0 0 0 0 0.590853 0 0 0 0 1 0 0 0 1 0"
					/>
					<feBlend
						mode="normal"
						in2="BackgroundImageFix"
						result="effect1_dropShadow_5_1171"
					/>
					<feBlend
						mode="normal"
						in="SourceGraphic"
						in2="effect1_dropShadow_5_1171"
						result="shape"
					/>
				</filter>
				<linearGradient
					id="paint0_linear_5_1171"
					x1="82.0911"
					y1="52.806"
					x2="175.571"
					y2="129.688"
					gradientUnits="userSpaceOnUse"
				>
					<stop stop-color="#5797FF" />
					<stop offset="1" stop-color="#005BD0" />
				</linearGradient>
				<linearGradient
					id="paint1_linear_5_1171"
					x1="127.751"
					y1="102.445"
					x2="188.351"
					y2="178.762"
					gradientUnits="userSpaceOnUse"
				>
					<stop stop-color="#5797FF" />
					<stop offset="1" stop-color="#005BD0" />
				</linearGradient>
				<linearGradient
					id="paint2_linear_5_1171"
					x1="174.107"
					y1="142.738"
					x2="207.808"
					y2="186.972"
					gradientUnits="userSpaceOnUse"
				>
					<stop stop-color="#FFCF6F" />
					<stop offset="1" stop-color="#FFA344" />
				</linearGradient>
			</defs>
		</svg>
	);
};

export default OrganizationLogo;
