import logo from "./assets/logo-medium.png";

// export default function LoginLogo() {
// 	return (
// 		<svg
// 			width="276"
// 			height="277"
// 			viewBox="0 0 276 277"
// 			fill="none"
// 			xmlns="http://www.w3.org/2000/svg"
// 		>
// 			<g filter="url(#filter0_d_5_1154)">
// 				<rect
// 					x="50.4426"
// 					y="49.1843"
// 					width="175.083"
// 					height="175.556"
// 					rx="35.2667"
// 					fill="white"
// 					shape-rendering="crispEdges"
// 				/>
// 				<circle cx="166.197" cy="163.412" r="32.3278" fill="#92BBFF" />
// 				<path
// 					d="M156.128 162.824H111.612C102.64 162.824 96.9212 153.3 101.259 145.486L112.746 124.778L123.493 105.388C127.979 97.3295 139.712 97.3295 144.198 105.388L154.994 124.778L160.17 134.106L166.48 145.486C170.819 153.3 165.1 162.824 156.128 162.824Z"
// 					fill="url(#paint0_linear_5_1154)"
// 				/>
// 				<path
// 					fill-rule="evenodd"
// 					clip-rule="evenodd"
// 					d="M87.7766 78.1843C82.0695 78.1843 77.4429 82.8109 77.4429 88.5181C77.4429 92.2836 79.4569 95.5787 82.4665 97.3849C83.8216 98.1982 84.9063 99.5361 84.9063 101.117L84.9063 161.46C84.9063 163.04 83.8215 164.378 82.4664 165.192C79.4567 166.998 77.4427 170.293 77.4427 174.059C77.4427 179.766 82.0692 184.392 87.7764 184.392C91.8433 184.392 95.3614 182.043 97.0478 178.628C97.8149 177.074 99.2365 175.781 100.969 175.781H167.588C169.321 175.781 170.742 177.074 171.509 178.628C173.196 182.043 176.714 184.392 180.781 184.392C186.488 184.392 191.114 179.766 191.114 174.059C191.114 170.452 189.266 167.276 186.466 165.428C185.211 164.599 184.225 163.307 184.225 161.804L184.225 100.773C184.225 99.2694 185.211 97.9774 186.465 97.1491C189.266 95.3006 191.114 92.1252 191.114 88.5182C191.114 82.8111 186.488 78.1845 180.781 78.1845C177.338 78.1845 174.289 79.8674 172.411 82.4553C171.574 83.6092 170.331 84.4993 168.905 84.4993L99.6521 84.4993C98.2264 84.4993 96.9832 83.6092 96.1459 82.4553C94.2679 79.8673 91.2188 78.1843 87.7766 78.1843ZM178.484 101.428C178.484 99.7717 177.297 98.3907 175.842 97.5979C173.973 96.5792 172.454 95.0004 171.509 93.087C170.742 91.5334 169.32 90.2403 167.588 90.2403H100.969C99.2367 90.2403 97.8152 91.5333 97.0481 93.0869C96.1669 94.8718 94.7855 96.3654 93.0869 97.3849C91.7319 98.1982 90.6473 99.536 90.6473 101.116L90.6473 161.46C90.6473 163.041 91.7318 164.379 93.0868 165.192C94.286 165.912 95.3271 166.868 96.1457 167.996C96.983 169.15 98.2262 170.04 99.6519 170.04L168.905 170.04C170.331 170.04 171.574 169.15 172.411 167.996C173.314 166.753 174.486 165.718 175.842 164.979C177.297 164.186 178.484 162.805 178.484 161.148L178.484 101.428Z"
// 					fill="url(#paint1_linear_5_1154)"
// 				/>
// 			</g>
// 			<defs>
// 				<filter
// 					id="filter0_d_5_1154"
// 					x="0.481518"
// 					y="0.692661"
// 					width="275.005"
// 					height="275.478"
// 					filterUnits="userSpaceOnUse"
// 					color-interpolation-filters="sRGB"
// 				>
// 					<feFlood flood-opacity="0" result="BackgroundImageFix" />
// 					<feColorMatrix
// 						in="SourceAlpha"
// 						type="matrix"
// 						values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
// 						result="hardAlpha"
// 					/>
// 					<feOffset dy="1.46944" />
// 					<feGaussianBlur stdDeviation="24.9806" />
// 					<feComposite in2="hardAlpha" operator="out" />
// 					<feColorMatrix
// 						type="matrix"
// 						values="0 0 0 0 0.339365 0 0 0 0 0.590853 0 0 0 0 1 0 0 0 1 0"
// 					/>
// 					<feBlend
// 						mode="normal"
// 						in2="BackgroundImageFix"
// 						result="effect1_dropShadow_5_1154"
// 					/>
// 					<feBlend
// 						mode="normal"
// 						in="SourceGraphic"
// 						in2="effect1_dropShadow_5_1154"
// 						result="shape"
// 					/>
// 				</filter>
// 				<linearGradient
// 					id="paint0_linear_5_1154"
// 					x1="112.675"
// 					y1="94.2282"
// 					x2="149.728"
// 					y2="165.087"
// 					gradientUnits="userSpaceOnUse"
// 				>
// 					<stop stop-color="#FFCF6F" />
// 					<stop offset="1" stop-color="#FFA344" />
// 				</linearGradient>
// 				<linearGradient
// 					id="paint1_linear_5_1154"
// 					x1="96.7618"
// 					y1="55.126"
// 					x2="150.485"
// 					y2="208.454"
// 					gradientUnits="userSpaceOnUse"
// 				>
// 					<stop stop-color="#5797FF" />
// 					<stop offset="1" stop-color="#005BD0" />
// 				</linearGradient>
// 			</defs>
// 		</svg>
// 	);
// }

export default function LoginLogo() {
	return <img src={logo}/>
}